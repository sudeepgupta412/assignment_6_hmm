Daniel Castillo Today at 7:22 AM
hey guys, working on the emissions params for BUY for 1a, . I got B1 right, but my B2 mean is wrong. For those that did via excel, do you move boundary more than one place in one iteration?




89 replies

Mjnari Richards  11 hours ago
Yes you have to move boundary more than one place in one iteration to get the correct answer

Tyler Bellows  11 hours ago
@Daniel Castillo How many iterations did it take you to converge? I only took 3 (but I'm having the same problem as you)

Mjnari Richards  11 hours ago
I believe I did it in 2

Mjnari Richards  11 hours ago
But don't hold that to a golden rule I don't remember for certain

Tyler Bellows  11 hours ago
But we have to take into account distance in terms of standard deviations, right?

Mjnari Richards  11 hours ago
Yes

Daniel Castillo  11 hours ago
I initially had 3, but experimented with moving only 1 or 2 per boundary

Daniel Castillo  11 hours ago
Are we allowed to swap numbers? For example, can we swap 2 numbers from one state with 2 numbers with another state? (edited) 

Mjnari Richards  10 hours ago
No there is no swapping assuming you mean two numbers trading their positions in a single iteration.
Also you have to do it from left to right to get the correct answer so swapping isn't feasible if you are doing it correctly

Tyler Bellows  10 hours ago
Whoops (edited) 

Mjnari Richards  10 hours ago
He asked us not to share mid examples

Mjnari Richards  10 hours ago
please delete

Daniel Castillo  10 hours ago
left to right, does that mean move boundary left to right or number left to right if it's tied?

Mjnari Richards  10 hours ago
I haven't done all 3 words yet but as far as BUY there are not any ties. I'm not certain what you should do in a tie

Mjnari Richards  10 hours ago
It means the same way that the video starts with the numbers in state 1, then checks state 2, then state 3, you need to do this in that order. The reason this means you could not have 2 numbers trading positions is you are not doing 2 stats at the same time. You're only checking 1 state at a time, for 1 direction of movement at a time

Tyler Bellows  10 hours ago
My bad. I didn't realize we couldn't share any actual numbers from part 1

Mjnari Richards  10 hours ago
No worries man

Mjnari Richards  10 hours ago
Just didn't want anyone getting in trouble

Tyler Bellows  10 hours ago
OK, so we can't leave any states empty right? So, if I had a state with only one value, even if it would fit better in another state, I cannot leave the state empty, so it stays?

Mjnari Richards  10 hours ago
Exactly

Tyler Bellows  10 hours ago
Sweet

Daniel Castillo  10 hours ago
got mine working through trial/error

Daniel Castillo  10 hours ago
think I understand

Tyler Bellows  10 hours ago
Gaa, I'm having a weird oscillation thing

Daniel Castillo  10 hours ago
I have only 2 iterations

Tyler Bellows  10 hours ago
OK

Tyler Bellows  10 hours ago
I'm definitely doing something wrong, just not sure what

Daniel Castillo  10 hours ago
took me awhile, but hopefully rest will be easier now that I have one down

Daniel Castillo  10 hours ago
only moved max of one number per boundary per iteration for B

Tyler Bellows  10 hours ago
So, you checked the two values on either side of the boundary, right?

Tyler Bellows  10 hours ago
With three possible outcomes:
Boundary doesn't move
Boundary moves left
Boundary moves right

Tyler Bellows  10 hours ago
Is that right?

Daniel Castillo  10 hours ago
correct and I attempt to move the boundary left before right

Tyler Bellows  10 hours ago
Ah OK

Daniel Castillo  10 hours ago
that's what I meant from above about "ties", if both values on either side would be better in opposite state

Tyler Bellows  10 hours ago
Gotcha

Mjnari Richards  10 hours ago
Oh interesting

Mjnari Richards  10 hours ago
If you're thinking about it from that perspective then I would say left gets priority

Mjnari Richards  10 hours ago
Sorry I am doing it from the perspective of the numbers so that didn't occur to me

Tyler Bellows  10 hours ago
Well I got BUY working! Wohoo!
:tada:
2


Jayesh Sharma  10 hours ago
I have two values that match

Jayesh Sharma  10 hours ago
but third one doesn't .. is it even possible

Mjnari Richards  10 hours ago
Rounding error?

Mjnari Richards  10 hours ago
Wait when you say values you're talking about for one word right? Like you got B1 and B2 correct but B3 wrong?

Jayesh Sharma  10 hours ago
I get B1 wrong, B2 right, B3 right

Jayesh Sharma  10 hours ago
B2 ==> mean, std-dev, correct,
B3 ==> mean, std-dev, correct (edited) 

Jayesh Sharma  10 hours ago
B1 mean and std-dev wrong

Mjnari Richards  10 hours ago
wow

Mjnari Richards  10 hours ago
As long as you are using all the numbers that shouldnt be possible

Mjnari Richards  10 hours ago
My thoughts are you probably:
Removed a number
Added a number
Calculated Mean and std wrong
But other then that idk. Might be easier to re-do then try to solve

Jayesh Sharma  10 hours ago
B1 as far as i know doesn't have long recurring digits for mean

Jayesh Sharma  10 hours ago
I used calculator to compute mean

Jayesh Sharma  10 hours ago
so may be it looks like digit in an out..

Mjnari Richards  10 hours ago
yeah see if you are missing a number in your calculation

Tyler Bellows  9 hours ago
Dang it, failing C though...

Tyler Bellows  9 hours ago
Two iterations for C?

Mjnari Richards  9 hours ago
I don’t remember. But I do remember C has some very close std devs, check those carefully

Daniel Castillo  9 hours ago
I have two iterations, but stuck on C2 also

Tyler Bellows  9 hours ago
Right now I get the mean/std dev for C1 but not C2

Daniel Castillo  9 hours ago
:same2same:

Daniel Castillo  9 hours ago
moving numbers right to left first (boundary left to right), up to 2 moves now per boundary

Jayesh Sharma  9 hours ago
Jesus christ, i copied one of the numbers incorrectly from the problem set :disappointed:

Jayesh Sharma  9 hours ago
it was off by one and messed up my B1

Tyler Bellows  9 hours ago
OK, well, now C3's mean is incorrect... but C1 and C2 are right... how?
:joy:
1


Jayesh Sharma  9 hours ago
i hate these do by hand exercises.. this is exactly anti-AI exercise

Jayesh Sharma  9 hours ago
Tyler check your numbers.. the one that you entered

Daniel Castillo  9 hours ago
Still can't get C2

Jayesh Sharma  9 hours ago
we mostly move boundaries.. but if there is error in entering numbers your means will be off even though your logic is right..

Tyler Bellows  9 hours ago
Got it. Entered wrong number in Excel
:this:
1


Tyler Bellows  9 hours ago
Whoops

Tyler Bellows  9 hours ago
@Daniel Castillo I had to do 3 iterations

Tyler Bellows  9 hours ago
And then I saw the light

Tyler Bellows  9 hours ago
Of course, it mocked me

Daniel Castillo  9 hours ago
got it thx
:+1:
1


Tyler Bellows  9 hours ago
Brutal lol

Daniel Castillo  9 hours ago
lol crazy what you miss from staring too long

Tyler Bellows  8 hours ago
Any luck on H? Stuck on H2

Mjnari Richards  8 hours ago
just got H, took a couple tries. Another one where you have to be real careful of the std devs

Jayesh Sharma  8 hours ago
still stuck on C

Daniel Castillo  8 hours ago
On H2 as well

Daniel Castillo  8 hours ago
@Mjnari Richards how many iterations? I'm at 3

Tyler Bellows  8 hours ago
Thank the maker. 3 iterations

Tyler Bellows  8 hours ago
Of death

Tyler Bellows  8 hours ago
It's like I want to submit but now I'm scared of the probabilities lol

Daniel Castillo  8 hours ago
:exploding_head: can't find problem, how many values are in your second state?

Tyler Bellows  8 hours ago
Final second state has 11 values

Daniel Castillo  8 hours ago
what? mine has 22... (edited) 

Jayesh Sharma  7 hours ago
i didn't even realize i worked on house thinking it was car.. :slightly_smiling_face: and now my house passes but car fails.. (edited) 

Jayesh Sharma  6 hours ago
finally passing unit test on 1a